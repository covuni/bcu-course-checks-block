<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block_bcu_course_checks
 * @copyright  2014 Michael Grant <michael.grant@bcu.ac.uk>
 * @copyright  2018 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Get course properties.
 *
 * @param int $courseid
 *
 * @return stdClass $course
 */
function block_bcu_course_checks_get_course_properties($courseid) {
    global $DB;
    $course = $DB->get_record('course', array('id' => $courseid));
    return $course;
}

/**
 * Check sections for the course.
 *
 * @param int $courseid
 *
 * @return array
 */
function block_bcu_course_checks_check_sections($courseid) {
    global $CFG;
    require_once($CFG->dirroot.'/course/lib.php');
    $status = array();
    $course = get_course($courseid);
    $courseinf = course_get_format($course)->get_course();
    $modinfo = get_fast_modinfo($course);
    $allsectiondetails = get_active_sections($courseid);

    foreach ($allsectiondetails['sectioninfo'] as $section => $value) {

        $checkthissection = true;

        // The variable numsections in topic / week course formats (possibly others) is removed from Moodle 3.3 onwards.
        // Ref: https://tracker.moodle.org/browse/MDL-57769.  We check numsections is set only for 3.2 and earlier.
        if (isset ($courseinf->numsections)) {
            if ($section > $courseinf->numsections) {
                // Means we found section higher than numsections (i.e. an orphaned activity in Moodle 3.2 and earlier).
                $checkthissection = false;
            }
        }

        if ($checkthissection) {
            if ($value->section > 0) {
                if ($value->name === null) {
                    $status[$value->section]['nameisset'] = false;
                    $status[$value->section]['name'] = get_string('sectionnameunnamed', 'block_bcu_course_checks');
                } else {
                    $status[$value->section]['nameisset'] = true;
                    $status[$value->section]['name'] = $value->name;
                }
                if (strlen($value->summary) === 0) {
                    $status[$value->section]['summary'] = false;
                } else {
                    $status[$value->section]['summary'] = true;
                }
                if (strlen($value->sequence) === 0) {
                    $status[$value->section]['content'] = false;
                } else {
                    $status[$value->section]['content'] = true;
                }
                if ($value->visible === "0") {
                    $status[$value->section]['visible'] = false;
                } else {
                    $status[$value->section]['visible'] = true;
                }
            }
        } else {
            break;
        }
    }
    return $status;
}

/**
 * Get course guide statuses for the specified course.
 *
 * @param   int $courseid
 *
 * @return  array All course guides found and their statuses
 */
function block_bcu_course_checks_get_courseguide_statuses($courseid) {
    global $CFG, $DB, $USER, $OUTPUT;
    require_once($CFG->dirroot.'/course/lib.php');
    require_once($CFG->libdir . '/completionlib.php');

    $course = get_course($courseid);
    $courseinf = course_get_format($course)->get_course();
    $modinfo = get_fast_modinfo($course);

    $checkresult = array();
    $templateresults = array();

    if (isset ($modinfo->instances['courseguide'])) {
        foreach ($modinfo->instances['courseguide'] as $cm) {
            if (!$cm->uservisible) {
                continue;
            }
            $assignmentsql = 'SELECT cg.* FROM {courseguide} c JOIN {mod_courseguide_guides} cg ' .
                             'ON c.id = cg.courseguideid WHERE c.id = ?';

            $courseguideguidesrecord = $DB->get_record_sql($assignmentsql, array('id' => $cm->instance));

            $templateresults[] = array(
                    'cmid' => $cm->id,
                    'cm_name' => $cm->name,
                    'cm_url'  => $cm->url,
                    'results' => $courseguideguidesrecord);
        }
    }

    return $templateresults;

}

/**
 * Get course format.
 *
 * @param int $courseid
 *
 * @return stdClass course format
 */
function block_bcu_course_checks_check_format($courseid) {
    global $CFG;
    require_once($CFG->dirroot.'/course/lib.php');
    $course = get_course($courseid);
    $courseinf = course_get_format($course)->get_course();
    return $courseinf;
}

/**
 * Get all block names.
 *
 * @return array blocknames
 */
function block_bcu_course_checks_get_all_blocks() {
    GLOBAL $DB, $CFG;
    if (!$blocks = $DB->get_records('block', array(), 'name ASC')) {
        print_error('noblocks', 'error');  // Should never happen.
    }
    foreach ($blocks as $blockid => $block) {
        $blockname = $block->name;
        if (file_exists("$CFG->dirroot/blocks/$blockname/block_$blockname.php")) {
            $blocknames[$blockname.'-'.get_string('pluginname', 'block_'.$blockname)]
            = get_string('pluginname', 'block_'.$blockname);
        } else {
            $blocknames[$blockname] = $blockname;
        }
    }
    core_collator::asort($blocknames);
    return $blocknames;
}

/**
 * Get all formats.
 *
 * @return array format names
 */
function block_bcu_course_checks_get_all_formats() {
    GLOBAL $CFG;
    require_once($CFG->dirroot.'/course/lib.php');
    $formats = get_sorted_course_formats(true);
    foreach ($formats as $format) {
        $formatnames[$format] = get_string('pluginname', "format_$format");
    }
    core_collator::asort($formatnames);
    return $formatnames;
}

/**
 * Get block statuses of all blocks present.
 *
 * @param class $course
 *
 * @return array status of blocks
 */
function block_bcu_course_checks_get_course_blocks($course) {
    GLOBAL $CFG, $PAGE, $DB;
    require_once($CFG->libdir. '/blocklib.php');
    $blockmanager = $PAGE->blocks;
    $blockmanager->load_blocks(true);
    $reqblocks = explode(',', get_config('bcu_course_checks', 'blocksenabled'));
    foreach ($reqblocks as $required) {
        $blockname = explode('-', $required);
        if ($blockmanager->is_block_present($blockname[0])) {
            $status[$blockname[1]] = true;
        } else {
            $status[$blockname[1]] = false;
        }
    }
    return $status;
}

/**
 * Get course overview files.
 *
 * @param int $courseid
 *
 * @return array files list
 */
function block_bcu_course_checks_get_course_overviewfiles($courseid) {
    GLOBAL $CFG, $COURSE;
    if ($COURSE instanceof stdClass) {
        require_once($CFG->libdir. '/coursecatlib.php');
        $course = new course_in_list($COURSE);
    }
    $courseimg = false;
    foreach ($course->get_course_overviewfiles() as $file) {
        $isimage = $file->is_valid_image();
        if ($isimage) {
            $courseimg = true;
        }
    }
    return array('status' => $courseimg, 'files' => $course->get_course_overviewfiles());
}

/**
 * Check guest access.
 *
 * @param int $courseid
 *
 * @return bool
 */
function block_bcu_course_checks_get_guest_access($courseid) {
    global $DB;
    $course = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'guest'));

    if ($course && $course->status == 1) {
        return true;
    }
    return false;
}

/**
 * Course clean to remove empty sections.
 *
 * @param int $courseid
 * @param int $cleaniterations
 *
 * @return int number of sections cleaned.
 */
function block_bcu_course_checks_course_cleanup($courseid, $cleaniterations) {
    global $CFG;
    require_once($CFG->dirroot.'/course/lib.php');
    $course = get_course($courseid);
    $courseinf = course_get_format($course)->get_course();
    $modinfo = get_fast_modinfo($course);
    $i = 0;

    $allsectiondetails = get_active_sections($courseid);

    if ($allsectiondetails['totalsections'] > 0) {
        foreach ($allsectiondetails['sectioninfo'] as $section => $thissection) {
            if ($section <= $allsectiondetails['totalsections']) {

                $sectioninf = $modinfo->get_section_info($i)->sequence;
                // Check that the section has no content and no summary assigned.
                if (strlen($sectioninf) === 0 && strlen($thissection->summary) === 0) {

                    // The last parameter below means only delete if it has no modules in it.
                    $result = course_delete_section($course, $section, false);

                    // Reduce numsections value if it exists (e.g if we are using Moodle 3.2 or below).
                    if (isset ($courseinf->numsections)) {
                        course_get_format($course)->update_course_format_options(
                                array('numsections' => $allsectiondetails['totalsections'] - 1));
                    }

                    $cleaniterations++;

                    // Just incase sections aren't deleted above and to avoid a potential endless loop
                    // (as experienced during testing).
                    if ($cleaniterations > ($allsectiondetails['totalsections'] + 1) ) {
                        break;
                    }
                    return block_bcu_course_checks_course_cleanup($courseid, $cleaniterations);
                }
            }
            $i++;
        }
    }
    return $cleaniterations;
}

/**
 * This gets section info and total number of sections (excluding section 0 which is always present).
 *
 * @param int $courseid
 *
 * @return array section info
 */
function get_active_sections($courseid) {
    $modinfo = get_fast_modinfo($courseid);
    $numsections = 0;
    $sectioninfo = $modinfo->get_section_info_all();
    foreach ($sectioninfo as $section => $thissection) {
        $numsections++;
    }

    $courseinf = course_get_format($courseid)->get_course();

    // If numsections is present, use that instead (Moodle version 3.2).
    if (isset ($courseinf->numsections) ) {
        $numsections = $courseinf->numsections;
    } else {
        --$numsections;
    }
    return array('sectioninfo' => $sectioninfo, 'totalsections' => $numsections);
}

/**
 * Check if course can be cleaned.
 *
 * @param int $courseid
 * @param int $cleaniterations
 *
 * @return int number of sections that can be cleaned
 */
function block_bcu_course_checks_can_cleanup($courseid, $cleaniterations = 0) {
    global $CFG, $DB;
    require_once($CFG->dirroot.'/course/lib.php');
    $course = get_course($courseid);
    $courseinf = course_get_format($course)->get_course();
    $modinfo = get_fast_modinfo($course);
    $i = 0;

    $validsections = true;

    $allsectiondetails = get_active_sections($courseid);

    if ($allsectiondetails['totalsections'] > 0) {
        foreach ($allsectiondetails['sectioninfo'] as $section => $thissection) {

            $checkthissection = true;

            // Old 3.2 numsections check (to ignore orphaned sections).
            if ( (isset ($courseinf->numsections)) && ($section > $courseinf->numsections) ) {
                $checkthissection = false;
            }

            if ($checkthissection) {
                $sectioninf = $modinfo->get_section_info($i)->sequence;

                // Check that the section has no content and no summary assigned.
                if (strlen($sectioninf) === 0 && strlen($thissection->summary) === 0) {
                    $cleaniterations++;
                }
            }
            $i++;
        }
    }
    return $cleaniterations;
}

/**
 * Get outdated assignments.
 *
 * @param stdClass $courseinfo
 *
 * @return array outdated assignments
 */
function block_bcu_course_checks_get_outdated_assignments($courseinfo) {
    global $DB, $OUTPUT;

    $anyfails = false;

    $content = html_writer::start_tag('fieldset', null, array('class' => 'clearfix collapsible'));

    if ($courseinfo->timecreated > $courseinfo->startdate) {
        $datecheck = $courseinfo->timecreated;
    } else {
        $datecheck = $courseinfo->startdate;
    }

    $assignmentssql = 'SELECT cm.id, a.name, a.duedate FROM {course_modules} cm JOIN {assign} a ON ' .
                      'cm.instance = a.id WHERE cm.course = ? AND a.duedate < ? AND cm.module = ?';

    $assignments = $DB->get_records_sql($assignmentssql, array($courseinfo->id, $datecheck, 1));
    foreach ($assignments as $assignment) {
        $anyfails = true;

        if (strlen($assignment->name) > 30) {
            $name = substr($assignment->name, 0, 30).'...';
        } else {
            $name = $assignment->name;
        }

        $content .= html_writer::tag('span', $name, array('class' => 'assignmenttitle', 'title' => $assignment->name)) .
                    html_writer::tag('span', html_writer::link(new moodle_url('/course/modedit.php',
                    array('update' => $assignment->id, 'return' => 1)), $OUTPUT->pix_icon('t/edit', '')),
                    array('class' => 'bcu_course_status'));
        $content .= html_writer::tag('span', userdate($assignment->duedate), array('class' => 'assignmentdate'));
        $content .= html_writer::empty_tag('hr', array('class' => 'assignmenthr'));
    }

    $content .= html_writer::end_tag('fieldset');

    return array('content' => $content, 'status' => $anyfails);
}

/**
 * Show all assignment dates in the module.
 *
 * @param stdClass $courseinfo a course to get the assignments from for the current user.
 *
 * @return array all assignments
 */
function block_bcu_course_checks_get_assignments($courseinfo) {
    global $DB, $OUTPUT;

    $anyfails = false;

    // For each course get mods.
    $modinfo = get_fast_modinfo($courseinfo->id);

    $assignmentlist = array();

    foreach ($modinfo->cms as $cm) {
        if ($cm->modname == 'assign') {
            $activitydates = block_bcu_course_checks_instance_activity_dates($courseinfo->id, $cm);

            $assignmentlist[] = array('name' => $cm->name, 'id' => $cm->id, 'activitydates' => $activitydates);
        }
    }

    $content = html_writer::start_tag('fieldset', null, array('class' => 'clearfix collapsible'));

    if ($courseinfo->timecreated > $courseinfo->startdate) {
        $datecheck = $courseinfo->timecreated;
    } else {
        $datecheck = $courseinfo->startdate;
    }

    foreach ($assignmentlist as $assignment) {

        if (strlen($assignment['name']) > 30) {
            $name = substr($assignment['name'], 0, 30) . '...';
        } else {
            $name = $assignment['name'];
        }
        $due = $assignment['activitydates']->timeclose;
        if ($assignment['activitydates']->extension) {
            $due = $assignment['activitydates']->extension;
        }
        $content .= html_writer::tag('span', $name, array('class' => 'assignmenttitle', 'title' => $name)).
                    html_writer::tag('span', html_writer::link(new moodle_url('/course/modedit.php',
                                     array('update' => $assignment['id'], 'return' => 1)),
                                    $OUTPUT->pix_icon('t/edit', '')), array('class' => 'bcu_course_status'));
        $content .= html_writer::tag('span', userdate($due), array('class' => 'assignmentdate'));
        $content .= html_writer::empty_tag('hr', array('class' => 'assignmenthr'));
    }

    $content .= html_writer::end_tag('fieldset');

    return array('content' => $content, 'status' => $anyfails);
}

 /**
  * Get the activity dates for a specific module instance.
  *
  * @param int $courseid
  * @param stdClass $mod
  * @param string $timeopenfld
  * @param string $timeclosefld
  *
  * @return bool|stdClass
  */
function block_bcu_course_checks_instance_activity_dates($courseid, $mod,
    $timeopenfld = 'allowsubmissionsfromdate', $timeclosefld = 'duedate') {

    global $DB, $USER;
    // Note: Caches all moduledates to minimise database transactions.
    static $moddates = array();
    if (!isset($moddates[$courseid . '_' . $mod->modname][$mod->instance])) {
        $timeopenfld = $mod->modname === 'quiz' ? 'timeopen' : ($mod->modname === 'lesson' ? 'available' : $timeopenfld);
        $timeclosefld = $mod->modname === 'quiz' ? 'timeclose' : ($mod->modname === 'lesson' ? 'deadline' : $timeclosefld);
        $sql = "-- Snap sql
        SELECT
        module.id,
        module.$timeopenfld AS timeopen,
        module.$timeclosefld AS timeclose";
        if ($mod->modname === 'assign') {
            $sql .= ",
                auf.extensionduedate AS extension
            ";
        }

        $params = [];
        $sql .= "  FROM {" . $mod->modname . "} module";
        if ($mod->modname === 'assign') {
            $params[] = $USER->id;
            $sql .= "
              LEFT JOIN {assign_user_flags} auf
                     ON module.id = auf.assignment
                    AND auf.userid = ?
             ";
        }
        $params[] = $courseid;
        $sql .= " WHERE module.course = ?";
        $result = $DB->get_records_sql($sql, $params);

        $moddates[$courseid . '_' . $mod->modname] = $result;
    }
    $modinst = $moddates[$courseid.'_'.$mod->modname][$mod->instance];
    if (!empty($modinst->timecloseover)) {
        $modinst->timeclose = $modinst->timecloseover;
        if ($modinst->timeopenover) {
            $modinst->timeopen = $modinst->timeopenover;
        }
    }
    return $modinst;

}